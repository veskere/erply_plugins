// =========================== ORDER technician communication comment field ====================

$(window).load(function() {
    var installationRowChildren = $("#installationFormRow").children();

    $(":input[name=eventNotes]").remove();
    installationRowChildren.eq(4).append("<textarea rows='5' id='eventNotes' style='resize: none; width: 50%'></textarea>");

});

// =========================== ORDER technician communication comment field  END====================

// =========================== JQUERY CHAIN GENERATOR ========================== 

function combineElementIdentifier(elementName) {
	return ':input[name="'+ elementName +'"]';
}

function combineElementSelector(elementIdentifier) {
	return "$('"+ elementIdentifier +"')";
}

function combineChain(methodName, chainCount) {
	var holder = "." + methodName + "()";
  
  holder = holder.repeat(chainCount);
  
  return holder;
}

function combineChainOnElement(elementName, methodName, chainCount) {
  var elementIdentifier = combineElementIdentifier(elementName);
  var elementSelector = combineElementSelector(elementIdentifier);
  var elementChain = combineChain(methodName, chainCount);
 
  return elementSelector + elementChain;
}

// ============================== JQUERY CHAIN GENERATOR END ===================

/*
*  hidingCases holds country, group and invoice type relations 
*  and which fields to hide for given invoice type
*/

var hidingCases = [
/*
	{
  "country": "EE",
  "groupId": 1,
  "invoiceTypeId": 9,
  "fields": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
  },
  {
  "country": "EE",
  "groupId": 1,
  "invoiceTypeId": 8,
  "fields": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23]
  },
  {
  "country": "EE",
  "groupId": 1,
  "invoiceTypeId": 1,
  "fields": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
  },
  {
  "country": "EE",
  "groupId": 1,
  "invoiceTypeId": 3,
  "fields":  [1, 2, 3, 4,  5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 23]
  },
*/
{
  "country": "EE",
  "groupId": 10,
  "invoiceTypeId": 9,
  "fields": [ 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 25, 26]
  },
  {
  "country": "EE",
  "groupId": 10,
  "invoiceTypeId": 8,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 25, 26]
  },
  {
  "country": "EE",
  "groupId": 10,
  "invoiceTypeId": 1,
  "fields": [ 2, 3,  4,  6, 7, 8, 9, 10, 11, 12,13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26]
  },
  {
  "country": "EE",
  "groupId": 10,
  "invoiceTypeId": 3,
  "fields":  [ 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 23, 25, 26]
  },
{
  "country": "EE",
  "groupId": 16,
  "invoiceTypeId": 9,
  "fields": [ 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 25, 26]
  },
  {
  "country": "EE",
  "groupId": 16,
  "invoiceTypeId": 8,
  "fields": [ 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 25, 26]
  },
  {
  "country": "EE",
  "groupId": 16,
  "invoiceTypeId": 1,
  "fields": [ 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26]
  },
  {
  "country": "EE",
  "groupId": 16,
  "invoiceTypeId": 3,
  "fields":  [ 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 23, 25, 26]
  },
{
  "country": "LV",
  "groupId": 13,
  "invoiceTypeId": 9,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 25, 26]
  },
  {
  "country": "LV",
  "groupId": 13,
  "invoiceTypeId": 8,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 25, 26]
  },
  {
  "country": "LV",
  "groupId": 13,
  "invoiceTypeId": 1,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26]
  },
  {
  "country": "LV",
  "groupId": 13,
  "invoiceTypeId": 3,
  "fields":  [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 23, 25, 26]
  },
{
  "country": "LT",
  "groupId": 12,
  "invoiceTypeId": 9,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 12,
  "invoiceTypeId": 8,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 12,
  "invoiceTypeId": 1,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 12,
  "invoiceTypeId": 3,
  "fields":  [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 23, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 13,
  "invoiceTypeId": 9,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 13,
  "invoiceTypeId": 8,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 22, 23, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 13,
  "invoiceTypeId": 1,
  "fields": [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 25, 26]
  },
  {
  "country": "LT",
  "groupId": 13,
  "invoiceTypeId": 3,
  "fields":  [1, 2, 3, 4,  6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 23, 25, 26]
  }
];

var fieldsToHide = [
	{"id": 1,"fieldName" : "invoice_address_id","parentCount" : 2},
	{"id": 2,"fieldName" : "invoice_orgper_idDat_contact","parentCount" : 2},
	{"id": 3,"fieldName" : "invoice_not_european_union","parentCount" : 2},
	{"id": 4,"fieldName" : "invoice_salepoint_id","parentCount" : 2},
	{"id": 5,"fieldName" : "invoice_pricelist_id","parentCount" : 2},
	{"id": 6,"fieldName" : "invoice_invattr_packer","parentCount" : 2},
	{"id": 7,"fieldName" : "invoice_project_idDat_text","parentCount" : 6},
	{"id": 8, "fieldName" : "invoice_currency_id","parentCount" : 2},
	{"id": 9, "fieldName" : "attribute_text_needsInstallation","parentCount" : 6},
	{"id": 10,"fieldName" : "invoice_opertype_id","parentCount" : 2},
	{"id": 11,"fieldName" : "invoice_transtype_id","parentCount" : 2},
	{"id": 12,"fieldName" : "invoice_purvtype_id","parentCount" : 2},
	{"id": 13,"fieldName" : "invoice_packnotes","parentCount" : 5},
	{"id": 14,"fieldName" : "invoice_triangularoperation","parentCount" : 2},
	{"id": 15, "fieldName" : "invoice_notes","parentCount" : 5},
	{"id": 16,"fieldName" : "invoice_private_notes","parentCount" : 5},
	{"id": 17,"fieldName" : "invoice_print_discount","parentCount" : 2},
	{"id": 18,"fieldName" : "invoice_print_original_names","parentCount" : 2},
	{"id": 19,"fieldName" : "serializedPageUrl","parentCount" : 1},
	{"id": 20, "fieldName" : "invoice_deadline","parentCount" : 2},
	{"id": 21, "fieldName" : "invoice_penalty","parentCount" : 2},
	{"id": 22, "fieldName" : "invoice_paytype_id","parentCount" : 2},
	{"id": 23,"fieldName" : "invoice_paystate_id","parentCount" : 2},
	{"id": 24, "fieldName" : "invoice_fob_addedvalue","parentCount" : 2},
	{"id": 25, "fieldName" : "invoice_address_id_payer","parentCount" : 2},
	{"id": 26, "fieldName" : "invoice_orgper_idDat_payer_text","parentCount" : 6}
];

// =========================  HIDING INVOICE FIELDS ============================

/*
*  Automatics of hiding fields works only for named fields. 
*  Works for any invoice type, for any group, for any country. 
*  
*  To hide a new field, add given field to fieldsToHide list. 
*  And then add the id of added list item to hidingCases
*  
*/

function getAccountFromUrl(url) {
	
  var afterHttps = url.substring(8, url.length);
  var urlArray = afterHttps.split('/');
  var accountNumber = urlArray[1];
  
  return accountNumber;
  
}

function getCountryFromAccount(accountNumber) {

	var country = "";
  
  if (accountNumber === "254438" || accountNumber === "330720") {
  	country = "EE"
  }
  
  if (accountNumber === "330521" || accountNumber === "330521") {
  	country = "LV"
  }
  
  if (accountNumber === "330505" || accountNumber === "355924") {
  	country = "LT"
  }
  
  return country;
  
}

function shouldHideFieldsForCurrentUser(currentCountry, currentUserGroupId, currentInvoiceTypeId) {
	
  var result = false;
  
  hidingCases.forEach(function(hidingCase) {
  
  	if (hidingCase.country == currentCountry && 
    		hidingCase.groupId == currentUserGroupId && 
    		hidingCase.invoiceTypeId == currentInvoiceTypeId) {
    	
      result = true;
    }
  
  });
	
	return result;
}

function getHidingCaseFields(currentCountry, currentUserGroupId, currentInvoiceTypeId) {
	
  var result = [];
  
  hidingCases.forEach(function(hidingCase) {
  
  	if (hidingCase.country == currentCountry && 
    		hidingCase.groupId == currentUserGroupId && 
    		hidingCase.invoiceTypeId == currentInvoiceTypeId) {
    	
      result = hidingCase.fields;
    }
  
  });

	return result;

}

function getFieldsToHide(currentCountry, currentUserGroupId, currentInvoiceTypeId) {
	
  var result = [];
  var fieldIds = getHidingCaseFields(currentCountry, currentUserGroupId, currentInvoiceTypeId);
 	
  fieldsToHide.forEach(function(field) {
    fieldIds.forEach(function(fieldId) {
    	
      if (field.id == fieldId) {
      	result.push(field);
      }
    
    });
  });
  
  return result;
  
}

function getElementsToHide(currentCountry, currentUserGroupId, currentInvoiceTypeId) {
	
  var result = [];
  var currentFieldsToHide = getFieldsToHide(currentCountry, currentUserGroupId, currentInvoiceTypeId);
  
  currentFieldsToHide.forEach(function(field) {
  
  	result.push(eval(combineChainOnElement(field.fieldName, "parent", field.parentCount)));
  
  });
  
  return result;
  
}

function hideNamedElements(currentCountry, currentUserGroupId, currentInvoiceTypeId) {

	var elementsToHide = getElementsToHide(currentCountry, currentUserGroupId, currentInvoiceTypeId);
  
  elementsToHide.forEach(function(element) {
  	element.hide();
  });
  
}

function hideElementsWithoutNames(invoiceTypeId, currentCountry) {
  if ( invoiceTypeId== 9  && country == "EE") {
    var editForm = $("div.editTable div.editForm").children();
    editForm.eq(9).hide();
  }

  if (invoiceTypeId== 1 && country == "EE" ) {
    var editForm = $("div.editForm").children();
    var tbody = editForm.eq(5).children();
    var cardPayment = tbody.children().eq(1);

    cardPayment.hide();
  }

  if (invoiceTypeId== 8 && country == "EE") {
    var editForm = $("div.editForm").children();
    var formContainer = editForm.eq(5);
    var cardPayment = formContainer.children().children().eq(1);

    cardPayment.hide();
  }

  if (invoiceTypeId== 3 && country == "EE") {
    var editForm = $("div.editForm").children();
    var formContainer6 = editForm.eq(6);

    formContainer6.hide();
  }

}

$(window).load(function() {
	console.log("hidinginvoices");
  if (section != "invoice") { return false;}
  
  var url = $(location).attr("href");
  var accountNumber = getAccountFromUrl(url);
  var currentCountry = getCountryFromAccount(accountNumber);
  var userGroupId = userGroupID;
  var invoiceTypeId = parseInt($(":input[name='invoice_type_id']").val());
  
  console.log("accountNumber " +accountNumber);
  console.log("currentCountry " +currentCountry);
  console.log("userGroupId " + userGroupId);
  console.log("invoiceTypeId " + invoiceTypeId);

  if (shouldHideFieldsForCurrentUser(currentCountry, userGroupId, invoiceTypeId)) {
  	
    hideNamedElements(currentCountry, userGroupId, invoiceTypeId);
    hideElementsWithoutNames(invoiceTypeId, currentCountry);
  }

});


// ===================================  HIDING INVOICE FIELDS END =====================================